Model of the system with an Attitude and Orbit Control Subsystem (AOCS) and Watchdog.
It is a simplified version the the model presented in https://es-static.fbk.eu/people/tonetta/publications/safecomp16.pdf.

With TimeEvent and ChangeEvent is is possible to express the asynchronous communication among components.

