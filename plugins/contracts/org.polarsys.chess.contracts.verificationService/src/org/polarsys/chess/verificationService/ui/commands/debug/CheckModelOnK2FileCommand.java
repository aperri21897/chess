package org.polarsys.chess.verificationService.ui.commands.debug;

import java.io.File;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.runtime.IProgressMonitor;
import org.polarsys.chess.service.core.model.ChessSystemModel;

import eu.fbk.eclipse.standardTools.KratosExecService.ui.services.KratosExecService;
import eu.fbk.eclipse.standardTools.KratosExecService.ui.utils.KratosDialogUtil;
import eu.fbk.eclipse.standardTools.KratosExecService.ui.utils.KratosDirectoryUtil;
import eu.fbk.eclipse.standardtools.utils.ui.commands.AbstractAsyncJobCommand;

public class CheckModelOnK2FileCommand extends AbstractAsyncJobCommand {
	

	private KratosExecService kratosExecService = KratosExecService.getInstance(ChessSystemModel.getInstance());
	private KratosDialogUtil kratosDialogUtil = KratosDialogUtil.getInstance();
	private KratosDirectoryUtil kratosDirectoryUtil = KratosDirectoryUtil.getInstance();
	private String resultFilePath;
	
	
	public CheckModelOnK2FileCommand() {
		super("Check Software Model on K2 File Command");
	}
	
	@Override
	public void execJobCommand(ExecutionEvent event, IProgressMonitor monitor) throws Exception {

		File smvFile = kratosDialogUtil.getK2FileFromFileDialog(kratosDirectoryUtil.getK2FileDirectory());
		String filePath = smvFile.getPath();
		resultFilePath = kratosDirectoryUtil.getCommandCheckModelCommandResultPath(smvFile.getName());		
		kratosExecService.checkSwModel(filePath, null, resultFilePath, true);
	}

}
