package org.polarsys.chess.verificationService.ui.commands;

import java.util.List;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.papyrus.editor.PapyrusMultiDiagramEditor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Model;
import org.polarsys.chess.chessmlprofile.Dependability.DependableComponent.Analysis;
import org.polarsys.chess.chessmlprofile.Dependability.DependableComponent.AnalysisContextElement;
import org.polarsys.chess.chessmlprofile.ParameterizedArchitecture.InstantiatedArchitectureConfiguration;
import org.polarsys.chess.contracts.profile.chesscontract.util.EntityUtil;
import org.polarsys.chess.core.util.uml.ResourceUtils;
import org.polarsys.chess.service.core.model.ChessSystemModel;
import org.polarsys.chess.service.core.model.UMLStateMachineModel;
import org.polarsys.chess.service.core.utils.AnalysisResultUtil;
import org.polarsys.chess.service.gui.utils.CHESSEditorUtils;
import org.polarsys.chess.service.gui.utils.SelectionUtil;

import eu.fbk.eclipse.standardTools.KratosExecService.ui.services.KratosExecService;
import eu.fbk.eclipse.standardTools.KratosExecService.ui.utils.KratosDirectoryUtil;
import eu.fbk.eclipse.standardtools.StateMachineTranslatorToSmv.ui.services.K2ExportServiceUI;
import eu.fbk.eclipse.standardtools.utils.core.utils.StringArrayUtil;
import eu.fbk.eclipse.standardtools.utils.ui.commands.AbstractJobCommand;
import eu.fbk.eclipse.standardtools.utils.ui.dialogs.SelectArchitectureConfigurationDialog;
import eu.fbk.tools.adapter.kratos.CheckSoftwareModel;

public class ModelCheckingKratosCommand extends AbstractJobCommand {

	private SelectionUtil selectionUtil = SelectionUtil.getInstance();
	private K2ExportServiceUI k2ExportService = K2ExportServiceUI.getInstance(ChessSystemModel.getInstance(),
			UMLStateMachineModel.getInstance());
	private KratosDirectoryUtil kratosDirectoryUtil = KratosDirectoryUtil.getInstance();
	private KratosExecService kratosExecService = KratosExecService.getInstance(ChessSystemModel.getInstance());
	private AnalysisResultUtil analysisResultUtil = AnalysisResultUtil.getInstance();
	private EList<String> conditions;
	private String k2Directory;
	private String fileWithPropertyPath;
	private String propertyFilePath;
	private String resultFilePath;
	private boolean showPopups;
	private Class umlSelectedComponent;
	private boolean commandExecuted;

	private InstantiatedArchitectureConfiguration selectedInstantiatedArchitectureConfiguration;
	private List<AnalysisContextElement> contextList;

	public ModelCheckingKratosCommand() {
		super("Check Software Model Command");
	}

	@Override
	public void execPreJobOperations(ExecutionEvent event, IProgressMonitor monitor) throws Exception {
		umlSelectedComponent = selectionUtil.getUmlComponentFromSelectedObject(event);
		k2Directory = kratosDirectoryUtil.getK2DirPath();
		showPopups = false;
		fileWithPropertyPath = kratosDirectoryUtil
				.getCommandInjectPropertyIntoModelResultPath(umlSelectedComponent.getName());
		resultFilePath = kratosDirectoryUtil.getCommandCheckModelCommandResultPath(umlSelectedComponent.getName());
		propertyFilePath = kratosDirectoryUtil.getCommandInjectPropertyIntoModelPropertyFile();

		Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		EList<InstantiatedArchitectureConfiguration> instantiatedArchitecures = EntityUtil.getInstance()
				.getInstantiatedArchitecureConfigurations(umlSelectedComponent);
		if ((instantiatedArchitecures != null) && (!instantiatedArchitecures.isEmpty())) {
			SelectArchitectureConfigurationDialog dialog = new SelectArchitectureConfigurationDialog(shell,
					ChessSystemModel.getInstance(), instantiatedArchitecures);
			dialog.open();
			if (dialog.goAhead()) {
				selectedInstantiatedArchitectureConfiguration = (InstantiatedArchitectureConfiguration) dialog
						.getSelectedAchitectureConfiguration();
			}
		}

		PapyrusMultiDiagramEditor editorPapyrus = CHESSEditorUtils.getCHESSEditor();
		Resource res = ResourceUtils.getUMLResource(editorPapyrus.getServicesRegistry());
		Model model = ResourceUtils.getModel(res);

		contextList = AnalysisResultUtil.getInstance().getAnalysisContexts(umlSelectedComponent,
				selectedInstantiatedArchitectureConfiguration, Analysis.MODEL_CHECKING_ANALYSIS, model);

	}

	@Override
	public void execJobCommand(ExecutionEvent event, IProgressMonitor monitor) throws Exception {

		// Compose the conditions
		String[] expression = new String[1]; // It will be filled by the
												// method

		String filePath = k2ExportService.exportSingleBlock(umlSelectedComponent, showPopups,
				umlSelectedComponent.getName(), k2Directory, monitor);
		commandExecuted = kratosExecService.executeModelChecking(filePath, fileWithPropertyPath, propertyFilePath,
				resultFilePath, false, true, expression, contextList);
		if (commandExecuted) {
			conditions = createConditions(expression[0].split("#"));
		}

	}

	@Override
	public void execPostJobOperations(ExecutionEvent event, NullProgressMonitor nullProgressMonitor) throws Exception {
		if (commandExecuted) {
			// Store the result
			analysisResultUtil.createOrUpdateAnalysisContext(Analysis.MODEL_CHECKING_ANALYSIS, conditions,
					resultFilePath, false, umlSelectedComponent, selectedInstantiatedArchitectureConfiguration, null);

			// Visualize the result
			analysisResultUtil.showResult(CheckSoftwareModel.FUNCTION_NAME, resultFilePath);
		}
	}

	private EList<String> createConditions(String[] expression) {
		BasicEList<String> conditions = new BasicEList<String>(); // It will be
																	// filled by
																	// the
																	// method
		// Store the type of check
		StringArrayUtil.addConditionKeyValue(conditions, AnalysisResultUtil.CHECK_TYPE, "ltl_spec_kratos");
		// Set the expression to be stored in the result
		if (expression != null) {
			StringArrayUtil.addConditionKeyValue(conditions, AnalysisResultUtil.PROPERTY, expression[0]);
		}
		return conditions;
	}

}
