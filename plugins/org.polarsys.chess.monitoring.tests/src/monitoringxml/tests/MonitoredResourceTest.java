/**
 */
package monitoringxml.tests;

import org.polarsys.chess.monitoring.monitoringxml.MonitoredResource;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Monitored Resource</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class MonitoredResourceTest extends TestCase {

	/**
	 * The fixture for this Monitored Resource test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MonitoredResource fixture = null;

	/**
	 * Constructs a new Monitored Resource test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MonitoredResourceTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Monitored Resource test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(MonitoredResource fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Monitored Resource test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MonitoredResource getFixture() {
		return fixture;
	}

} //MonitoredResourceTest
