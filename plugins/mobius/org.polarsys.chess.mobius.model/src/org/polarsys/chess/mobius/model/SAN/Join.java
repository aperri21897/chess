/**
 */
package org.polarsys.chess.mobius.model.SAN;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Join</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.polarsys.chess.mobius.model.SAN.SANModelPackage#getJoin()
 * @model
 * @generated
 */
public interface Join extends ComposedNode {

} // Join
