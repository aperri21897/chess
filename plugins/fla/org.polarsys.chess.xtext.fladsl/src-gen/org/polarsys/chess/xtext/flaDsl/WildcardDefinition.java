/**
 */
package org.polarsys.chess.xtext.flaDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Wildcard Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.polarsys.chess.xtext.flaDsl.FlaDslPackage#getWildcardDefinition()
 * @model
 * @generated
 */
public interface WildcardDefinition extends Definitions
{
} // WildcardDefinition
