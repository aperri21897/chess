/**
 */
package org.polarsys.chess.xtext.flaDsl.impl;

import org.eclipse.emf.ecore.EClass;

import org.polarsys.chess.xtext.flaDsl.FlaDslPackage;
import org.polarsys.chess.xtext.flaDsl.WildcardDefinition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Wildcard Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class WildcardDefinitionImpl extends DefinitionsImpl implements WildcardDefinition
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected WildcardDefinitionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return FlaDslPackage.Literals.WILDCARD_DEFINITION;
  }

} //WildcardDefinitionImpl
