/*******************************************************************************
 * Copyright (C) 2020 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 ******************************************************************************/
package org.polarsys.chess.cdo.providers;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import org.eclipse.core.resources.IProject;

public class CHESSProjectListLabelProvider extends LabelProvider {


	public Image getImage(Object element) {
		return null;
	}


	public String getText(Object element) {
		IProject resource = (IProject) element;
		return resource.getName();
	}

}
