/**
 */
package org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.papyrus.sysml.blocks.Block;

import org.eclipse.uml2.uml.Event;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Trigger Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.FunctionTriggerEvent#getTriggeredBlock <em>Triggered Block</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.FunctionTriggerEvent#getBase_Event <em>Base Event</em>}</li>
 * </ul>
 *
 * @see org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.AsyncCommunicationPackage#getFunctionTriggerEvent()
 * @model
 * @generated
 */
public interface FunctionTriggerEvent extends EObject {
	/**
	 * Returns the value of the '<em><b>Triggered Block</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Triggered Block</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Triggered Block</em>' reference.
	 * @see #setTriggeredBlock(Block)
	 * @see org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.AsyncCommunicationPackage#getFunctionTriggerEvent_TriggeredBlock()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Block getTriggeredBlock();

	/**
	 * Sets the value of the '{@link org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.FunctionTriggerEvent#getTriggeredBlock <em>Triggered Block</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Triggered Block</em>' reference.
	 * @see #getTriggeredBlock()
	 * @generated
	 */
	void setTriggeredBlock(Block value);

	/**
	 * Returns the value of the '<em><b>Base Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Event</em>' reference.
	 * @see #setBase_Event(Event)
	 * @see org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.AsyncCommunicationPackage#getFunctionTriggerEvent_Base_Event()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Event getBase_Event();

	/**
	 * Sets the value of the '{@link org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.FunctionTriggerEvent#getBase_Event <em>Base Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Event</em>' reference.
	 * @see #getBase_Event()
	 * @generated
	 */
	void setBase_Event(Event value);

} // FunctionTriggerEvent
