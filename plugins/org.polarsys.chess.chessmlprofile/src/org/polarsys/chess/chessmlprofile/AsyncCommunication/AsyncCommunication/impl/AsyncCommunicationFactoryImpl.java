/**
 */
package org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AsyncCommunicationFactoryImpl extends EFactoryImpl implements AsyncCommunicationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AsyncCommunicationFactory init() {
		try {
			AsyncCommunicationFactory theAsyncCommunicationFactory = (AsyncCommunicationFactory)EPackage.Registry.INSTANCE.getEFactory(AsyncCommunicationPackage.eNS_URI);
			if (theAsyncCommunicationFactory != null) {
				return theAsyncCommunicationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new AsyncCommunicationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AsyncCommunicationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case AsyncCommunicationPackage.FUNCTION_TRIGGER_EVENT: return createFunctionTriggerEvent();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionTriggerEvent createFunctionTriggerEvent() {
		FunctionTriggerEventImpl functionTriggerEvent = new FunctionTriggerEventImpl();
		return functionTriggerEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AsyncCommunicationPackage getAsyncCommunicationPackage() {
		return (AsyncCommunicationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static AsyncCommunicationPackage getPackage() {
		return AsyncCommunicationPackage.eINSTANCE;
	}

} //AsyncCommunicationFactoryImpl
