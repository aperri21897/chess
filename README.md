# Eclipse CHESS  
CHESS implements the CHESS UML/SysML profile, a specialization of the Modeling and Analysis of Real-Time and Embedded Systems (MARTE) profile, by producing extensions to Papyrus that provide component-based engineering methodology and tool support for the development of high-integrity embedded systems in different domains like satellite on board systems. 

## Developer resources

  * [CHESS Website](https://www.eclipse.org/chess/start.html)
  * [CHESS devel Website](https://projects.eclipse.org/projects/polarsys.chess)
  * [Forum](https://www.eclipse.org/forums/index.php/f/529/)
  * Mailing list: Join our [developer list](https://accounts.eclipse.org/mailing-list/chess-dev)
  * Bugs? [GitLab] (https://gitlab.eclipse.org/eclipse/chess/chess/-/issues)(previous bug traking system was [BugZilla](https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Chess)) is where to report them

## Contact

Contact the project developers via the project's "dev" list.

* chess-dev@eclipse.org

## How to contribute

The contributor guide can be found [here](https://gitlab.eclipse.org/eclipse/chess/chess/-/blob/devel/CONTRIBUTING.md).

