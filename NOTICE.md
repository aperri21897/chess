# Notices for Eclipse CHESS

This content is produced and maintained by the Eclipse CHESS project.

* Project home: https://projects.eclipse.org/projects/polarsys.chess

## Trademarks

Eclipse and CHESS are trademarks of the Eclipse Foundation.

## Copyright

All content is the property of the respective authors or their employers. For
more information regarding authorship of content, please consult the listed
source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0

SPDX-License-Identifier: EPL-2.0

## Source Code

The project maintains the following source code repositories:

* https://git.eclipse.org/r/chess/chess.git

## Third-party Content

This project leverages the following third party content.

SDE (Syntax-derived editor) 

* License: EPL-2.0 AND EPL-1.0 AND Apache-2.0 

EST (Eclipse Standard Tools) 

* License: EPL-1.0 AND Apache-2.0 AND MIT-CMU AND CC-BY-4.0 

## Cryptography

Content may contain encryption software. The country in which you are currently
may have restrictions on the import, possession, and use, and/or re-export to
another country, of encryption software. BEFORE using any encryption software,
please check the country's laws, regulations and policies concerning the import,
possession, or use, and re-export of encryption software, to see if this is
permitted.
